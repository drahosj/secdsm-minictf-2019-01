require 'sinatra/base'
require 'haml'
require 'open-uri'

class ImageGrabber < Sinatra::Base
  def initialize
    @words = IO.readlines('wordlist.txt').map{|w| w.capitalize.chomp}
    @seed = ENV["SUPER_SECRET_SEED"].to_i
    super
  end

  def new_filename(name)
    ext = name.split('.').last
    return @words.sample(4).join + '.' + ext
  end

  def encrypt(data)
    rng = Random.new(@seed)
    return data.bytes.map{|b| b ^ rng.rand(255)}.pack("c*")
  end

  not_found do
    haml :err_404
  end

  get '/i/:image' do
    img = File.join(settings.public_folder, "../i/#{params[:image]}")
    begin
      dat = IO.read(img)

      content_type File.extname(img), default: 'application/octet-stream'
      headers['Content-Length'] = dat.length

      return encrypt(dat)
    rescue Errno::ENOENT
      status 404
      return haml :err_404
    end
  end

  get '/' do
    haml :index
  end

  get '/grab' do
    haml :grab
  end

  post '/grab' do
    url = params[:url]

    unless url =~ /https?:.*\.(png|jpg|jpeg|svg|gif)/
      @url = url
      @toast = haml :toast_invalid, layout: false
      return haml :grab
    end

    open(url, 'r') do |i|
      @name = new_filename(url)

      open('i/' + @name, 'w') do |o|
        o.write(encrypt(i.read))
      end
    end

    @toast = haml :toast_success, layout: false
    haml :grab
  end
end
