require 'optparse'
require 'ostruct'

options = OpenStruct.new
OptionParser.new do |opts|
  opts.banner = "Usage: archiver [options]"

  opts.on('-u', '--user USER', 'User on archive server') do |u|
    options.user = u
  end

  opts.on('-p', '--password PASSWORD', 'Password on archive server') do |p|
    options.password = p
  end

  opts.on('-h', '--host HOST', 'Archive server hostname') do |h|
    options.host = h
  end

  if options.user.nil? or options.password.nil? or options.host.nil?
    puts opts
  end
end.parse!

def archive(options)
  puts "Archiving"
  h = options.host
  u = options.user
  p = options.password

  `sshpass -p '#{p}' scp -r i #{u}@#{h}:`
end

def clean_old
  `find i -type f -mmin +5 -exec rm -f {} \\;`
end

while true do
  archive(options)
  clean_old
  sleep 15
end
